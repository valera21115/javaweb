package by.training.simpleserver.service.impl;

import by.training.simpleserver.dao.InMemoryTrainCar;
import by.training.simpleserver.entity.TrainCar;
import by.training.simpleserver.service.SortService;

import java.util.*;

public class SimpleSortService implements SortService {
    private Map<String , Comparator> comparatorMap=new HashMap<>();
    private List<TrainCar> trainCars;

    public SimpleSortService(InMemoryTrainCar inMemoryTrainCar){
        this.trainCars=inMemoryTrainCar.getTrainCars();

        this.comparatorMap.put("byLuggageDown",COMPARE_BY_LUGGAGE_DOWN);
        this.comparatorMap.put("byLuggageUp",COMPARE_BY_LUGGAGE_UP);
        this.comparatorMap.put("byPassengersUp",COMPARE_BY_PASSENGERS_UP);
        this.comparatorMap.put("byPassengersDown",COMPARE_BY_PASSENGERS_DOWN);
    }


    public void sort(String comp){
        Collections.sort(this.trainCars,comparatorMap.get(comp));
    }

    public Comparator<TrainCar> COMPARE_BY_LUGGAGE_DOWN  = (first, second) -> (second.getLuggage()-first.getLuggage());

    public Comparator<TrainCar> COMPARE_BY_LUGGAGE_UP  = Comparator.comparingInt(TrainCar::getLuggage);

    public Comparator<TrainCar> COMPARE_BY_PASSENGERS_DOWN  = (first, second) -> (second.getPassengers()-first.getPassengers());

    public Comparator<TrainCar> COMPARE_BY_PASSENGERS_UP  = Comparator.comparingInt(TrainCar::getPassengers);


}
