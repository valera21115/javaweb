package by.training.simpleserver.controller.command.impl;

import by.training.simpleserver.controller.command.AppCommand;

import java.util.Map;

public class DefaultCommand implements AppCommand {
    final private String name = "Default";

    @Override
    public String perform(Map<String, String> requests) {

        return "<h1>Hello dear user!</h1>";
    }

    @Override
    public String getName() {
        return name;
    }
}
