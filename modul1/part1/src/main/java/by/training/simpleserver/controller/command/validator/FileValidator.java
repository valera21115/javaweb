package by.training.simpleserver.controller.command.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringTokenizer;

public class FileValidator {
    private final static Logger fileValidatorLogger = LogManager.getLogger(FileValidator.class);

    public String validate(String path) throws IOException {

        if (!path.matches("[A-z]:(\\\\\\w+[^<>|.*:\"/]+)+\\.[^,\\.\\\\/\"':;*-+()&?]+\\w+")) {
            fileValidatorLogger.info("incorrect path");
            return "<h1>incorrect path</h1>";
        }

        if (Files.notExists(Paths.get(path))) {
            fileValidatorLogger.info("file not exist");
            return "<h1>file not exist</h1>";
        }

        if (!path.endsWith(".txt") && !path.endsWith(".csv")) {
            fileValidatorLogger.info("incorrect type of file");
            return "<h1>incorrect type of file</h1>";
        }

        if (Paths.get(path).toFile().length() == 0) {
            fileValidatorLogger.info("Empty file");
            return "<h1>Empty file</h1>";
        }


        long lineCount = Files.lines(Paths.get(path)).count();


        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(path));) {

            StringTokenizer tokenizer;
            StringBuilder stringBuilder = new StringBuilder();

            bufferedReader.readLine();
            String incorrectParams = "Incorrect format data";

            for (long i = 1; i < lineCount; i++) {
                stringBuilder.append(bufferedReader.readLine());
                if (!stringBuilder.toString().matches("(\\w+\\W){3}\\w+")) {
                    fileValidatorLogger.info("Incorrect format data");
                    return incorrectParams;
                }
                tokenizer = new StringTokenizer(stringBuilder.toString(), ",.;*?");
                tokenizer.nextToken();
                if (Long.parseLong(tokenizer.nextToken()) <= 0) {
                    fileValidatorLogger.info("Incorrect param. Id must be natural number");
                    return incorrectParams;
                }
                if (Integer.parseInt(tokenizer.nextToken()) <= 0) {
                    fileValidatorLogger.info("Incorrect param. Passengers must be natural number");
                    return incorrectParams;
                }
                if (Integer.parseInt(tokenizer.nextToken()) <= 0) {
                    fileValidatorLogger.info("Incorrect param. Luggage must be natural number");
                    return incorrectParams;
                }
                stringBuilder.delete(0, stringBuilder.length());
            }
        } catch (Exception e) {
            fileValidatorLogger.info(e);
            return "Incorrect params";
        }

        return null;
    }
}
