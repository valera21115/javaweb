package by.training.simpleserver.controller.command.impl;

import by.training.simpleserver.ApplicationConstants;
import by.training.simpleserver.controller.command.AppCommand;
import by.training.simpleserver.entity.TrainCar;
import by.training.simpleserver.service.TrainCarService;

import java.util.Map;

public class ShowTrainCommand implements AppCommand {
    final private String name = "SHOW TRAIN";
    private TrainCarService trainCarService;

    public String getName() {
        return name;
    }

    public ShowTrainCommand(TrainCarService inMemoryTrainCarService) {
        this.trainCarService = inMemoryTrainCarService;
    }

    @Override
    public String perform(Map<String, String> requests) {

        StringBuilder stringBuilder = new StringBuilder();

        for (TrainCar trainCar : trainCarService.findAllTrainCars()) {
            stringBuilder.append("<form method='POST'>");
            stringBuilder.append(trainCar.getName() + " |passengers: "
                    + trainCar.getPassengers() + "|luggage: "
                    + trainCar.getLuggage() + " |id: "
                    + trainCar.getId()
                    + "|");
            stringBuilder.append("<input type='hidden' name='id' value='" + trainCar.getId() +
                    "'/>");
            stringBuilder.append("<input type='hidden' name='" + ApplicationConstants.COMMAND_PARAM +
                    "' value='delete'");
            stringBuilder.append(trainCar.getLuggage());
            stringBuilder.append("'/>");
            stringBuilder.append("<input type='submit' value='DELETE'/>");
            stringBuilder.append("</form>");


        }
        stringBuilder.append("<form method='POST'>" +
                "<input type=\"text\" name=\"name\" value=\"\" placeholder=\"Name\" required  ><br>" +
                "<input type=\"number\" name=\"id\" value=\"\" placeholder=\"ID\" required ><br>" +
                "<input type=\"number\" name=\"passengers\" value=\"\" placeholder=\"Passengers\" required ><br>" +
                "<input type=\"number\" name=\"luggage\" value=\"\" placeholder=\"Luggage\" required ><br>" +
                "<input type=\"hidden\" name=\"command\" value=\"add\">" +
                "<input type=\"submit\" value=\"ADD\"></form>\n");

        stringBuilder.append("<form method='POST'>" +
                "<input type=\"text\" name=\"path\" value=\"\" placeholder=\"Path to File\" required  ><br>" +
                "<input type=\"hidden\" name=\"command\" value=\"load\">" +
                "<input type=\"submit\" value=\"LOAD\"></form>\n");

        stringBuilder.append("<form method='GET'>" +
                "<input type=\"hidden\" name=\"command\" value=\"sort\">" +
                "<input type=\"hidden\" name=\"comp\" value=\"byLuggageDown\">" +
                "<input type=\"submit\" value=\"SORT BY LUGGAGE DOWN\"></form>\n");

        stringBuilder.append(
                "<form method='GET'>" +
                        "<input type=\"hidden\" name=\"command\" value=\"sort\">" +
                        "<input type=\"hidden\" name=\"comp\" value=\"byLuggageUp\">" +
                        "<input type=\"submit\" value=\"SORT BY LUGGAGE UP\"></form>\n");

        stringBuilder.append(
                "<form method='GET'>" +
                        "<input type=\"hidden\" name=\"command\" value=\"sort\">" +
                        "<input type=\"hidden\" name=\"comp\" value=\"byPassengersUp\">" +
                        "<input type=\"submit\" value=\"SORT BY PASSENGERS UP\"></form>\n");

        stringBuilder.append(
                "<form method='GET'>" +
                        "<input type=\"hidden\" name=\"command\" value=\"sort\">" +
                        "<input type=\"hidden\" name=\"comp\" value=\"byPassengersDown\">" +
                        "<input type=\"submit\" value=\"SORT BY PASSENGERS DOWN\"></form>\n");
        return stringBuilder.toString();
    }
}
