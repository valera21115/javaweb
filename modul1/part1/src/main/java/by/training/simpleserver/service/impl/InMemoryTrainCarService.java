package by.training.simpleserver.service.impl;

import by.training.simpleserver.dao.InMemoryTrainCar;
import by.training.simpleserver.entity.TrainCar;
import by.training.simpleserver.service.TrainCarService;

import java.util.*;

public class InMemoryTrainCarService implements TrainCarService {

    private List<TrainCar> trainCars;

    public InMemoryTrainCarService(InMemoryTrainCar inMemoryTrainCar) {
       this.trainCars=inMemoryTrainCar.getTrainCars();
    }

    @Override
    public List<TrainCar> findAllTrainCars() {
        return trainCars;
    }

    @Override
    public String addTrainCar(long id, String name,  int passengers, int luggage) {
        this.trainCars.add(new TrainCar(name,id, passengers,luggage));
        return "TrainCar Added";
    }

    @Override
    public String deleteTrainCar(long id){

        ListIterator<TrainCar> iterator = trainCars.listIterator();
        while (iterator.hasNext()){
            TrainCar trainCar = iterator.next();
            if(trainCar.getId()==id){
                iterator.remove();
                return "TrainCar Deleted";
            }
        }
        return "TrainCar not Deleted. Incorrect id";
    }

}


