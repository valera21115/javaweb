package by.training.simpleserver.dao;

import by.training.simpleserver.entity.TrainCar;

import java.util.ArrayList;
import java.util.List;

public class SimpleInMemoryTrainCar implements InMemoryTrainCar {
    List<TrainCar> trainCars=new ArrayList<>();

    public SimpleInMemoryTrainCar() {
        for(int i=1;i<10;i++){
            long id = i + 1;
            String name = "TrainCar" + i;
            trainCars.add(new TrainCar(name,id, (int)(5+Math.random()*20),(int)(7+Math.random()*10)));
        }
    }

    public List<TrainCar> getTrainCars() {
        return trainCars;
    }
}
