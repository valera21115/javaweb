package by.training.simpleserver;

import by.training.simpleserver.controller.SimpleHandler;
import by.training.simpleserver.controller.command.*;
import by.training.simpleserver.controller.command.impl.*;
import by.training.simpleserver.dao.InMemoryTrainCar;
import by.training.simpleserver.dao.SimpleInMemoryTrainCar;
import by.training.simpleserver.service.*;
import by.training.simpleserver.service.impl.InMemoryTrainCarService;
import by.training.simpleserver.service.impl.SimpleLoadFileService;
import by.training.simpleserver.service.impl.SimpleSortService;
import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

public class ServerApplication {

    public static void main(String[] args) throws IOException {
        final InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", 40000);
        HttpServer server = HttpServer.create(inetSocketAddress, 0);

        Logger logger = LogManager.getLogger(ServerApplication.class);
        InMemoryTrainCar inMemoryTrainCar = new SimpleInMemoryTrainCar();
        TrainCarService trainCarService = new InMemoryTrainCarService(inMemoryTrainCar);
        LoadFileService loadFileService = new SimpleLoadFileService(inMemoryTrainCar);
        SortService sortService = new SimpleSortService(inMemoryTrainCar);

        Map<String, AppCommand> commandStringMap = new HashMap<>();
        commandStringMap.put("show train", new ShowTrainCommand(trainCarService));
        commandStringMap.put("delete", new DeleteTrainCarCommand(trainCarService));
        commandStringMap.put("add", new AddTrainCarCommand(trainCarService));
        commandStringMap.put("sort", new SortCommand(sortService));
        commandStringMap.put("load", new LoadFileCommand(loadFileService));
        commandStringMap.put("default", new DefaultCommand());

        CommandPattern commandPattern = new CommandPattern(commandStringMap);


        server.createContext("/train", new SimpleHandler(commandPattern));

        server.start();
        logger.info("Server started on " + server.getAddress().toString());
    }


}
