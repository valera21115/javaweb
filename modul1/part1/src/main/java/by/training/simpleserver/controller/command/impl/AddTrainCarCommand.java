package by.training.simpleserver.controller.command.impl;

import by.training.simpleserver.controller.command.AppCommand;
import by.training.simpleserver.service.TrainCarService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class AddTrainCarCommand implements AppCommand {
    final private String name = "ADD";
    final private static Logger addTrainCarCommandLogger = LogManager.getLogger(AddTrainCarCommand.class);
    private TrainCarService trainCarService;

    public String getName() {
        return name;
    }


    public AddTrainCarCommand(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    @Override
    public String perform(Map<String, String> requests) {

        addTrainCarCommandLogger.info("addTrainCarCommand start");

        long id = Long.parseLong(requests.get("id"));
        String nameTrainCar = requests.get("name");
        short passengers = Short.parseShort(requests.get("passengers"));
        int luggage = Integer.parseInt(requests.get("luggage"));

        if (id > 0 && passengers > 0 && luggage > 0) {
            this.trainCarService.addTrainCar(id, nameTrainCar, passengers, luggage);
        } else {
            addTrainCarCommandLogger.info("Incorrect params");
            return "<h1>Incorrect params</h1>";
        }
        addTrainCarCommandLogger.info("TrainCar added");
        return "<h1>TrainCar added</h1>";
    }
}

