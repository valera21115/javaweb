package by.training.simpleserver.service;

public interface SortService {
    void sort(String comp);
}
