package by.training.simpleserver.entity;

public class TrainCar {


    private String name;
    private  long id;
    private int passengers;
    private int luggage;

    public TrainCar( String name,long id, int passengers, int luggage) {
        this.id=id;
        this.name = name;
        this.passengers=passengers;
        this.luggage=luggage;
    }


    public String getName() {
        return name;
    }
    public int getPassengers() {
        return passengers;
    }

    public int getLuggage(){return luggage;}
    @Override
    public String toString() {
        return "TrainCar{" +
                " name='" + name + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }
}
