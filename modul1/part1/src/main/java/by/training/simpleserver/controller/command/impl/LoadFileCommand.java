package by.training.simpleserver.controller.command.impl;

import by.training.simpleserver.controller.command.AppCommand;
import by.training.simpleserver.service.LoadFileService;
import by.training.simpleserver.controller.command.validator.FileValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Optional;

public class LoadFileCommand implements AppCommand {
    final private String name = "LOAD";
    final private static Logger loadFileCommandLogger = LogManager.getLogger(LoadFileCommand.class);
    private LoadFileService loadFileService;

    public LoadFileCommand(LoadFileService loadFileService) {
        this.loadFileService = loadFileService;
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public String perform(Map<String, String> requests) {

        loadFileCommandLogger.info("loadFileCommand start");
        String path = null;
        String validateResult = null;
        FileValidator validator = new FileValidator();
        try {
            path = URLDecoder.decode(requests.get("path"), StandardCharsets.UTF_8.toString());
            validateResult = validator.validate(path);
        } catch (UnsupportedEncodingException e) {
            loadFileCommandLogger.info(e);
        } catch (IOException e) {
            loadFileCommandLogger.info(e);
        }

        String result = Optional.ofNullable(validateResult).orElse(loadFileService.loadFile(path));
        loadFileCommandLogger.info(result);
        return result;

    }
}
