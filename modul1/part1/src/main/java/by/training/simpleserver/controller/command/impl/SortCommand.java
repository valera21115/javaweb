package by.training.simpleserver.controller.command.impl;

import by.training.simpleserver.controller.command.AppCommand;
import by.training.simpleserver.service.SortService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class SortCommand implements AppCommand {
    final private String name = "SORT";

    final private static Logger sortCommandLogger = LogManager.getLogger(SortCommand.class);

    public String getName() {
        return name;
    }

    private SortService sortService;


    public SortCommand(SortService sortService) {
        this.sortService = sortService;
    }

    @Override
    public String perform(Map<String, String> requests) {
        sortCommandLogger.info("sortCommand start");

        sortService.sort(requests.get("comp"));
        sortCommandLogger.info("Sorted " + requests.get("comp"));
        return "<h1>Sorted " + requests.get("comp") + "</h1>";
    }
}
