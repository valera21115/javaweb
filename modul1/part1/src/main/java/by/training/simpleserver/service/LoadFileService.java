package by.training.simpleserver.service;

public interface LoadFileService {

    String loadFile(String path);
}
