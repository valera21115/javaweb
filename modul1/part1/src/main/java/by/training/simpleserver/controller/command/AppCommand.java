package by.training.simpleserver.controller.command;


import java.util.Map;

public interface AppCommand {

   String perform(Map<String, String> requests);
   String getName();
}