package by.training.simpleserver.controller;

import by.training.simpleserver.controller.command.CommandPattern;
import by.training.simpleserver.ApplicationConstants;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URI;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

public class SimpleHandler implements HttpHandler {

    private CommandPattern commands;
    final private Logger simpleHandlerlogger = LogManager.getLogger(SimpleHandler.class);

    public SimpleHandler(CommandPattern commands) {
        this.commands = commands;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        URI logger = exchange.getRequestURI();
        simpleHandlerlogger.info("Received incoming request: " + requestMethod + " URI " + logger.toString());

        String query = logger.getQuery();
        simpleHandlerlogger.info("Queried: " + query);


        InputStream resourceAsStream = this.getClass().getResourceAsStream("/example.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        String template = bufferedReader.lines().collect(Collectors.joining());


        String view = MessageFormat.format(template, handleRequest(exchange));
        exchange.sendResponseHeaders(200, view.length());

        OutputStream responseBody = exchange.getResponseBody();
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();
    }


    private String[] handleRequest(HttpExchange exchange) throws IOException {
        String[] response = new String[2];
        Map<String, String> params = parseRequest(exchange);
        try {
            String commandName = Optional.ofNullable(params.get(ApplicationConstants.COMMAND_PARAM)).orElse("DEFAULT");
            response[0] = commands.getCommand(commandName).perform(params);
            response[1] = commands.getCommand("SHOW TRAIN").perform(params);
        } catch (Exception e) {
            response[0] = commands.getCommand("SHOW TRAIN").perform(params);
            response[1] = commands.getCommand("Default").perform(params);
            simpleHandlerlogger.info(e);
        }
        return response;
    }


    private Map<String, String> parseRequest(HttpExchange exchange) throws IOException {
        StringBuilder buffer = new StringBuilder();
        StringTokenizer tokenizer;
        if ("POST".equals(exchange.getRequestMethod())) {
            InputStream inputStream = exchange.getRequestBody();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            tokenizer = new StringTokenizer(bufferedReader.readLine());
        } else {
            String s = Optional.ofNullable(exchange.getRequestURI().getQuery()).orElse("");
            tokenizer = new StringTokenizer(s);
        }

        if (tokenizer.hasMoreTokens()) {
            buffer.append(tokenizer.nextToken());
        }

        Map<String, String> paramsMap = new HashMap<>();
        String str = buffer.toString();
        if (str == null) str = "";
        if (!str.equals("")) {
            String[] params = str.split("[&=]");

            for (int i = 0; i < params.length; i = i + 2) {
                paramsMap.put(params[i], params[i + 1]);
            }
        }
        return paramsMap;
    }

}