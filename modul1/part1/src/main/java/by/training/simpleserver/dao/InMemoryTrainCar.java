package by.training.simpleserver.dao;

import by.training.simpleserver.entity.TrainCar;

import java.util.List;

public interface InMemoryTrainCar {
    List<TrainCar> getTrainCars();
}
