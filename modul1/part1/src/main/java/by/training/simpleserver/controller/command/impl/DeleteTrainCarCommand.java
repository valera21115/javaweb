package by.training.simpleserver.controller.command.impl;

import by.training.simpleserver.controller.command.AppCommand;
import by.training.simpleserver.service.TrainCarService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class DeleteTrainCarCommand implements AppCommand {
    final private String name = "DELETE";
    final private static Logger deleteTrainCarCommandLogger = LogManager.getLogger(DeleteTrainCarCommand.class);
    private TrainCarService trainCarService;

    public String getName() {
        return name;
    }

    public DeleteTrainCarCommand(TrainCarService trainCarService) {
        this.trainCarService = trainCarService;
    }

    @Override
    public String perform(Map<String, String> requests) {
        deleteTrainCarCommandLogger.info("DeleteCommand start");
        String id = requests.get("id");

        String result = this.trainCarService.deleteTrainCar(Long.parseLong(id));
        deleteTrainCarCommandLogger.info(result);
        return result;

    }
}
