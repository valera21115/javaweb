package by.training.simpleserver.controller.command;

import java.util.Map;

public class CommandPattern {
    private Map<String,AppCommand> commandsMap;

    public CommandPattern(Map<String,AppCommand> commandsMap) {
        this.commandsMap = commandsMap;
    }

    public AppCommand getCommand(String commandName) {
        return commandsMap.get(commandName.toLowerCase());
    }
    public AppCommand getCommands(String commandName) {
        return commandsMap.get(commandName.toLowerCase());
    }

}
