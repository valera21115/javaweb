package by.training.simpleserver.service.impl;

import by.training.simpleserver.dao.InMemoryTrainCar;
import by.training.simpleserver.entity.TrainCar;
import by.training.simpleserver.service.LoadFileService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class SimpleLoadFileService implements LoadFileService {

    final private static Logger loadFileServiceLogger = LogManager.getLogger(SimpleLoadFileService.class);
    private List<TrainCar> trainCars;

    public SimpleLoadFileService(InMemoryTrainCar inMemoryTrainCar){

        this.trainCars=inMemoryTrainCar.getTrainCars();
    }

    public String loadFile(String path){
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(path))) {

            List<TrainCar> tempTrainCars = new ArrayList<>();
            StringTokenizer tokenizer;
            StringBuilder stringBuffer = new StringBuilder();


            long lineCount = Files.lines(Paths.get(path)).count();
            bufferedReader.readLine();
            for (long i = 1; i < lineCount; i++) {
                stringBuffer.append(bufferedReader.readLine());
                tokenizer = new StringTokenizer(stringBuffer.toString(), ",.;*?");

                tempTrainCars.add(new TrainCar(tokenizer.nextToken()
                        , Long.parseLong(tokenizer.nextToken())
                        , Integer.parseInt(tokenizer.nextToken())
                        , Integer.parseInt(tokenizer.nextToken())));

                stringBuffer.delete(0, stringBuffer.length());
            }

            trainCars.addAll(tempTrainCars);

        } catch (Exception e) {
            loadFileServiceLogger.info(e);
        }
        return "Load success";
    }
}
