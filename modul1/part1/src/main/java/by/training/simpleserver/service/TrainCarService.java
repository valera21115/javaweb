package by.training.simpleserver.service;

import by.training.simpleserver.entity.TrainCar;

import java.util.List;

public interface TrainCarService {

    List<TrainCar> findAllTrainCars();
    String addTrainCar(long id, String name, int passengers, int luggage);
    String deleteTrainCar(long id);
}