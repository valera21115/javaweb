package by.training.simpleserver;

import by.training.simpleserver.controller.command.AppCommand;
import by.training.simpleserver.controller.command.CommandPattern;
import by.training.simpleserver.controller.command.impl.*;
import by.training.simpleserver.dao.InMemoryTrainCar;
import by.training.simpleserver.dao.SimpleInMemoryTrainCar;
import by.training.simpleserver.service.LoadFileService;
import by.training.simpleserver.service.SortService;
import by.training.simpleserver.service.TrainCarService;
import by.training.simpleserver.service.impl.InMemoryTrainCarService;
import by.training.simpleserver.service.impl.SimpleLoadFileService;
import by.training.simpleserver.service.impl.SimpleSortService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ServerApplicationTest {

    Map<String, AppCommand> commandStringMap = new HashMap<>();
    InMemoryTrainCar inMemoryTrainCar = new SimpleInMemoryTrainCar();
    TrainCarService trainCarService = new InMemoryTrainCarService(inMemoryTrainCar);
    LoadFileService loadFileService = new SimpleLoadFileService(inMemoryTrainCar);
    SortService sortService = new SimpleSortService(inMemoryTrainCar);
    CommandPattern commandPattern = new CommandPattern(commandStringMap);


    @Before
    public void setUp() {
        commandStringMap.put("show train", new ShowTrainCommand(trainCarService));
        commandStringMap.put("delete", new DeleteTrainCarCommand(trainCarService));
        commandStringMap.put("add", new AddTrainCarCommand(trainCarService));
        commandStringMap.put("sort", new SortCommand(sortService));
        commandStringMap.put("load", new LoadFileCommand(loadFileService));
        commandStringMap.put("default", new DefaultCommand());
    }


    @Test
    public void addCommandTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("name", "Taet");
        requestMap.put("id", "666");
        requestMap.put("passengers", "666");
        requestMap.put("luggage", "999");

        int oldSize = trainCarService.findAllTrainCars().size();
        AppCommand command = commandStringMap.get("add");
        command.perform(requestMap);


        int newSize = trainCarService.findAllTrainCars().size();
        assertEquals(oldSize + 1, newSize);
    }

    ;


    @Test
    public void deleteCommandTest() throws IOException {
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", "2");

        AppCommand command = commandStringMap.get("delete");
        int oldSize = trainCarService.findAllTrainCars().size();
        command.perform(requestMap);
        int newSize = trainCarService.findAllTrainCars().size();
        assertEquals(oldSize, newSize + 1);
    }


    @Test
    public void deleteCommandIncorrectIdTest() {
        String actual = "TrainCar not Deleted. Incorrect id";
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("id", "666");

        AppCommand command = commandStringMap.get("delete");
        String result = command.perform(requestMap);
        assertEquals(result, actual);
    }

    @Test
    public void LoadIncorrectPathTest() {
        String actual = "<h1>incorrect path</h1>";
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("path", "/Incorrect path");
        AppCommand command = commandStringMap.get("load");
        String answer = command.perform(requestMap);
        assertEquals(answer, actual);
    }

    @Test
    public void LoadNotExistFileCommandTest() {
        String actual = "<h1>file not exist</h1>";
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("path", "F:\\document12.docx");

        AppCommand command = commandStringMap.get("load");
        String answer = command.perform(requestMap);
        assertEquals(answer, actual);
    }

    @Test
    public void LoadIncorrectTypeFileTest() {
        String actual = "<h1>incorrect type of file</h1>";
        Map<String, String> requestMap = new HashMap<>();
        String path = requestMap.put("path", "D:\\JavaWeb\\modul1\\part1\\src\\test\\resource\\test1.docx");
        AppCommand command = commandStringMap.get("load");
        String answer = command.perform(requestMap);
        assertEquals(answer, actual);
    }

    @Test
    public void LoadIncorrectDataFileTest() {
        String actual = "Incorrect params";
        Map<String, String> requestMap = new HashMap<>();
        String path = requestMap.put("path", "D:\\JavaWeb\\modul1\\part1\\src\\test\\resource\\test2.txt");
        AppCommand command = commandStringMap.get("load");
        String answer = command.perform(requestMap);
        assertEquals(answer, actual);
    }

    @Test
    public void LoadCorrectDataFileTest() {
        String actual = "Load success";
        Map<String, String> requestMap = new HashMap<>();
        String path = requestMap.put("path", "D:\\JavaWeb\\modul1\\part1\\src\\test\\resource\\test3.csv");
        AppCommand command = commandStringMap.get("load");
        String answer = command.perform(requestMap);
        assertEquals(answer, actual);
    }

    @After
    public void after() {
        commandStringMap.clear();
    }
}