package by.training.drugs.controller.servlets;

import by.training.drugs.service.impl.ParseStAXService;
import org.xml.sax.SAXException;

import  by.training.drugs.service.ParseFileService;
import java.io.IOException;
import java.text.ParseException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

public class AddStAXServlet extends HttpServlet {
    ParseFileService parseFileService=new ParseStAXService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path=req.getParameter("path");
        
        try {
            parseFileService.parse(path);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XMLStreamException | ParseException e) {
            e.printStackTrace();
        }
        req.setAttribute(parseFileService.show(),parseFileService);
        req.getRequestDispatcher("/index.jsp").forward(req, resp);

    }
}
