package by.training.drugs.controller.servlets;

import by.training.drugs.service.impl.ParseDOMService;
import by.training.drugs.service.impl.ParseSAXServise;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import  by.training.drugs.service.ParseFileService;
import java.io.IOException;
import java.text.ParseException;

public class AddSAXServlet extends HttpServlet {
    ParseFileService parseFileService=new ParseSAXServise();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path=req.getParameter("path");
        try {
            parseFileService.parse(path);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XMLStreamException | ParseException e) {
            e.printStackTrace();
        }
        resp.getWriter().write(parseFileService.show());

    }
}
