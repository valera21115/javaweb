package by.training.drugs.entity;

import java.util.Date;

public class Certificate {
    private long id;
    private Date dateOfIssue;
    private Date dateOfExpiry;
    private String organization;

    public Certificate(long id,Date dateOfIssue, Date dateOfExpiry, String organization){
        this.id=id;
        this.dateOfIssue=dateOfIssue;
        this.dateOfExpiry=dateOfExpiry;
        this.organization=organization;
    }
}
