package by.training.drugs.entity;

import java.util.Date;
import java.util.List;
import java.util.Map;
import by.training.drugs.entity.Certificate;
import by.training.drugs.entity.Dosage;

public class Drug {
    private String name;
    private String pharm;
    private String group;
    private String version;
    private String aPackage;
    private int count;
    private String price;
    private List<String> analogs;
    private Dosage dosage;
    private Certificate certificate;

     Drug() {
    }

    public Drug(String name, String pharm, String group, String version, String aPackage, List<String> analogs, Dosage dosage, Certificate certificate) {
        this.name = name;
        this.pharm = pharm;
        this.group = group;
        this.version = version;
        this.aPackage = aPackage;
        this.analogs = analogs;
        this.dosage = dosage;
        this.certificate = certificate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPharm(String pharm) {
        this.pharm = pharm;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setaPackage(String aPackage) {
        this.aPackage = aPackage;
    }

    public void setAnalogs(List<String> analogs) {
        this.analogs = analogs;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    @Override
    public String toString() {
        return "Drug{" +
                "name='" + name + '\'' +
                ", pharm='" + pharm + '\'' +
                ", group='" + group + '\'' +
                ", version='" + version + '\'' +
                ", aPackage='" + aPackage + '\'' +
                ", count=" + count +
                ", price='" + price + '\'' +
                ", analogs=" + analogs +
                ", dosage=" + dosage +
                ", certificate=" + certificate +
                '}';
    }

    public static class Builder{
        private Drug drug;

        public Builder(){
            drug=new Drug();
        }

        public Builder setName(String name) {
            drug.name = name;
            return this;
        }

        public Builder setPharm(String pharm){
            drug.pharm=pharm;
            return this;
        }

        public Builder setGroup(String group){
            drug.group=group;
            return this;
        }

        public Builder setVersion(String version){
            drug.version=version;
            return this;
        }

        public Builder setPackage(String aPackage){
            drug.aPackage=aPackage;
            return this;
        }

        public Builder setCount(int count) {
            drug.count=count;
            return this;
        }

        public Builder setPrice(String price){
            drug.price=price;
            return this;
        }

        public Builder setAnalogs(List<String> analogs){
            drug.analogs=analogs;
            return this;
        }

        public Builder setDosage(Dosage dosage){
            drug.dosage=dosage;
            return this;
        }

        public Builder setCertificate(Certificate certificate){
            drug.certificate=certificate;
            return this;
        }

        public Drug build() {
            return drug;
        }
    }

}
