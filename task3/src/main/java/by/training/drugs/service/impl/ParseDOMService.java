package by.training.drugs.service.impl;

import by.training.drugs.dao.DataDAO;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import by.training.drugs.dao.DAO;
import by.training.drugs.entity.Certificate;
import by.training.drugs.entity.Dosage;
import by.training.drugs.entity.Drug;
import by.training.drugs.service.ParseFileService;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class ParseDOMService implements ParseFileService {
    private static Drug.Builder drugBuilder;
   private DAO DAO=new DataDAO();
    //private static List<Drug> drugs = new ArrayList<>();

    @Override
    public  void parse(String path) throws ParserConfigurationException, IOException, SAXException, ParseException {
        StringBuilder out =new StringBuilder();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(path));
        NodeList drugElements = document.getDocumentElement().getElementsByTagName("drug");
        for (int i = 0; i < drugElements.getLength(); i++) {
            drugBuilder = new Drug.Builder();
            Node employee = drugElements.item(i);

            NamedNodeMap attributes = employee.getAttributes();

            List<String> analogs = new ArrayList<>();
          //  for(int j=0;j<2;j++){
                analogs.add(String.valueOf(attributes.getNamedItem("analogs").getChildNodes().item(0).getNodeValue()));
         //   }
            Dosage dosage = new Dosage(attributes.getNamedItem("dosage").getChildNodes().item(0).getNodeValue()
                    , attributes.getNamedItem("dosage").getChildNodes().item(1).getNodeValue());

            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
            Date dateofIssue = null;
            Date dateOfExpiry = null;
            dateofIssue=ft.parse(attributes.getNamedItem("certificate").getChildNodes().item(1).getNodeValue());
            dateOfExpiry=ft.parse(attributes.getNamedItem("certificate").getChildNodes().item(2).getNodeValue());
            Certificate certificate = new Certificate(Long.parseLong(attributes.getNamedItem("certificate").getChildNodes().item(0).getNodeValue())
                    , dateofIssue
                    , dateOfExpiry
                    , attributes.getNamedItem("certificate").getChildNodes().item(3).getNodeValue());

            drugBuilder.setName(attributes.getNamedItem("name").getNodeValue());
                    drugBuilder.setPharm(attributes.getNamedItem("pharm").getNodeValue());
                    drugBuilder.setGroup(attributes.getNamedItem("group").getNodeValue());
                    drugBuilder.setVersion(attributes.getNamedItem("version").getNodeValue());
                    drugBuilder.setPackage(attributes.getNamedItem("package").getNodeValue());
                    drugBuilder.setCount(Integer.parseInt(attributes.getNamedItem("count").getNodeValue()));
                    drugBuilder.setPrice(attributes.getNamedItem("price").getNodeValue());
                    drugBuilder.setAnalogs(analogs);
                    drugBuilder.setDosage(dosage);
                    drugBuilder.setCertificate(certificate);
            DAO.addDrug(drugBuilder.build());

        }
    }

    @Override
    public String show(){
        StringBuilder stringBuilder=new StringBuilder();
        for(Drug drug: DAO.getData()){
            stringBuilder.append(drug.toString());
        }
        return stringBuilder.toString();
    }
}
