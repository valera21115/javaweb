package by.training.drugs.service.impl;

import by.training.drugs.dao.DataDAO;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import by.training.drugs.dao.DAO;
import by.training.drugs.entity.Certificate;
import by.training.drugs.entity.Dosage;
import by.training.drugs.entity.Drug;
import by.training.drugs.service.ParseFileService;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ParseSAXServise implements ParseFileService {
    private static DAO DAO=new DataDAO();
   // private static Drug.Builder drugBuilder;

    @Override
    public void parse(String path) throws ParserConfigurationException, IOException, SAXException {
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        XMLHandler handler = new XMLHandler();
        parser.parse(new File(path), handler);

    }

    private static class XMLHandler extends DefaultHandler {
        String content = null;
        List<String> analogs;
        Drug.Builder drugBuilder;
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            drugBuilder = new Drug.Builder();
            if (qName.equals("drug")) {
                drugBuilder.setName(attributes.getValue("name"));
                drugBuilder.setPharm(attributes.getValue("pharm"));
                drugBuilder.setGroup(attributes.getValue("group"));
                drugBuilder.setVersion(attributes.getValue("version"));
                drugBuilder.setPackage(attributes.getValue("package"));
                drugBuilder.setCount(Integer.parseInt(attributes.getValue("count")));
                drugBuilder.setPrice(attributes.getValue("price"));
            }
            if (qName.equals("dosage")) {
                Dosage dosage = new Dosage(attributes.getValue("measuring"), attributes.getValue("period"));
                drugBuilder.setDosage(dosage);
            }
            if (qName.equals("certificate")) {
                SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
                Date dateofIssue = null;
                Date dateOfExpiry = null;
                try {
                    dateofIssue=ft.parse(attributes.getValue("dateOfIssue"));
                    dateOfExpiry=ft.parse(attributes.getValue("dateOfIssue"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Certificate certificate = new Certificate(Long.parseLong(attributes.getValue("id"))
                        , dateofIssue
                        , dateOfExpiry
                        , attributes.getValue("organization"));
                drugBuilder.setCertificate(certificate);
            }
            if (qName.equals("analogs")) {
                analogs = new ArrayList<>();
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {

            if (qName.equals("name")) {
                analogs.add(content);
            }
            if (qName.equals("analogs")) {
                drugBuilder.setAnalogs(analogs);
            }
            if (qName.equals("drug")) {
                DAO.addDrug(drugBuilder.build());
            }

        }


        @Override
        public void characters(char[] ch, int start, int length) {
            content = String.copyValueOf(ch, start, length).trim();
        }

    }

    @Override
    public String show() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Drug drug : DAO.getData()) {
            stringBuilder.append(drug.toString());
        }
        return stringBuilder.toString();
    }
}
