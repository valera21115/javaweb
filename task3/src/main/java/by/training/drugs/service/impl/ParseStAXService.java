package by.training.drugs.service.impl;

import by.training.drugs.dao.DataDAO;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import by.training.drugs.dao.DAO;
import by.training.drugs.entity.Certificate;
import by.training.drugs.entity.Dosage;
import by.training.drugs.entity.Drug;
import by.training.drugs.service.ParseFileService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParseStAXService implements ParseFileService {
    private static DAO DAO=new DataDAO();
    private static Drug.Builder drugBuilder;

    @Override
    public void parse(String path) throws ParserConfigurationException, IOException, SAXException, XMLStreamException, ParseException {
        List<String> analogs = new ArrayList<>();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader;


        reader = factory.createXMLStreamReader(new FileInputStream(path));


        while (reader.hasNext()) {
            int event = reader.next();
            switch (event) {
                case XMLStreamConstants.START_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "drug":
                            drugBuilder = new Drug.Builder();
                            drugBuilder.setName(reader.getAttributeValue("", "name"));
                            drugBuilder.setPharm(reader.getAttributeValue("", "pharm"));
                            drugBuilder.setGroup(reader.getAttributeValue("", "group"));
                            drugBuilder.setVersion(reader.getAttributeValue("", "version"));
                            drugBuilder.setPackage(reader.getAttributeValue("", "package"));
                            drugBuilder.setCount(Integer.parseInt(reader.getAttributeValue("", "count")));
                            drugBuilder.setPrice(reader.getAttributeValue("", "price"));
                        case "analogs":
                            analogs = new ArrayList<>();
                            break;
                        case "name":
                            analogs.add(reader.getElementText());
                            break;
                        case "dosage":
                            Dosage dosage = new Dosage(reader.getAttributeValue("", "measuring")
                                    , reader.getAttributeValue("", "period"));
                            drugBuilder.setDosage(dosage);
                            break;
                        case "certificate":
                            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
                            Date dateofIssue = null;
                            Date dateOfExpiry = null;
                            dateofIssue=ft.parse(reader.getAttributeValue("", "dateOfIssue"));
                            dateOfExpiry=ft.parse(reader.getAttributeValue("", "dateOfExpiry"));
                            Certificate certificate = new Certificate(Long.parseLong(reader.getAttributeValue("", "id"))
                                    , dateofIssue
                                    , dateOfExpiry
                                    , reader.getAttributeValue("", "organization"));
                            drugBuilder.setCertificate(certificate);
                    }
                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "drug":
                            DAO.addDrug(drugBuilder.build());
                            break;
                        case "analogs":
                            drugBuilder.setAnalogs(analogs);
                            break;
                    }
                    break;
            }
        }


    }

    @Override
    public String show() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Drug drug : DAO.getData()) {
            stringBuilder.append(drug.toString());
        }
        return stringBuilder.toString();
    }
}
