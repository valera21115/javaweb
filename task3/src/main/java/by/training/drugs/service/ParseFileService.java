package by.training.drugs.service;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.text.ParseException;

public interface ParseFileService {
    void parse(String path) throws ParserConfigurationException, IOException, SAXException, XMLStreamException, ParseException;
    String show();
}
