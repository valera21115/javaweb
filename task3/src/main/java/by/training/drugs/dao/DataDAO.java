package by.training.drugs.dao;

import by.training.drugs.entity.Drug;
import java.util.ArrayList;
import java.util.List;

public class DataDAO implements DAO {
    List<Drug> data= new ArrayList<>();

    public DataDAO() {

    }

    public DataDAO(List<Drug> data) {
        this.data = data;
    }

    @Override
    public List<Drug> getData() {
        return this.data;
    }

    public void addDrug(Drug drug) {
        this.data.add(drug);
    }


}
