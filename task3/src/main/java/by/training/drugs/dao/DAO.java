package by.training.drugs.dao;

import by.training.drugs.entity.Drug;
import java.util.List;

public interface DAO {
    List<Drug> getData();
    void addDrug(Drug drug);

}
